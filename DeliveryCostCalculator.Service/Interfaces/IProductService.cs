﻿using DeliveryCostCalculator.Data.Models;
using DeliveryCostCalculator.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryCostCalculator.Service.Interfaces
{
    public interface IProductService
    {
        List<ProductEntity> GetProducts();
        ProductEntity GetProduct(int id);
        void AddProduct(ProductEntity product);
    }
}
