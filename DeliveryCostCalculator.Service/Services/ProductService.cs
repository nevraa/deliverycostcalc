﻿using AutoMapper;
using DeliveryCostCalculator.Data;
using DeliveryCostCalculator.Data.Models;
using DeliveryCostCalculator.Model;
using DeliveryCostCalculator.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliveryCostCalculator.Service.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductService _productService;

        public ProductService(IProductService productService)
        {
            _productService = productService;
        }

        public void AddProduct(ProductEntity productEntity)
        {
            Product result;
            using (var _unitOfWork = new UnitOfWork())
            {
                var product = Mapper.Map<ProductEntity, Product>(productEntity);
                result = _unitOfWork.ProductRepository.Insert(product);
            }
        }

        public ProductEntity GetProduct(int id)
        {
            using (var _unitOfWork = new UnitOfWork())
            {
                var product = _unitOfWork.ProductRepository.GetById(id);

                if (product != null)
                {
                    var productEntity = Mapper.Map<Product, ProductEntity>(product);
                    return productEntity;
                }
                else
                    return null;
            }
        }

        public List<ProductEntity> GetProducts()
        {
            using (var _unitOfWork = new UnitOfWork())
            {
                var products = _unitOfWork.ProductRepository.GetAsNoTrackingAll() as List<Product>;
                var productEntityList = Mapper.Map<List<Product>, List<ProductEntity>>(products);
                return productEntityList;
            }
        }
    }
}
