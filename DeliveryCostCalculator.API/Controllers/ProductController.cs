﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliveryCostCalculator.Model;
using DeliveryCostCalculator.Service.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DeliveryCostCalculator.API.Controllers
{
    [Produces("application/json")]
    [Route("api/Product")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost]
        [Route("Product")]
        public ProductEntity AddProduct(ProductEntity productEntity)
        {
            var addedProduct = _productService.AddProduct(productEntity);
        }
    }
}