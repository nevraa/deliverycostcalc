﻿namespace DeliveryCostCalculator.Model
{
    public class CategoryEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }

        public CategoryEntity(int id, string title)
        {
            Id = id;
            Title = title;
        }
    }
}
