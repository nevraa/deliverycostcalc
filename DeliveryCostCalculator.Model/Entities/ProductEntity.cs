﻿namespace DeliveryCostCalculator.Model
{
    public class ProductEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public double Price { get; set; }
        public ProductEntity Category { get; set; }

        public ProductEntity()
        {

        }

        public ProductEntity(int id, string title, double price, ProductEntity category)
        {
            Id = id;
            Title = title;
            Price = price;
            Category = category;
        }
    }
}
