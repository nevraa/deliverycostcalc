﻿namespace DeliveryCostCalculator.Model
{
    public class CouponEntity
    {
        public string Title { get; set; }
        public double PurchaseAmount { get; set; }
        public double Rate { get; set; }
        public DiscountType DiscountType { get; set; }

        public CouponEntity(double purchaseAmount, double rate, DiscountType discountType)
        {
            Title = string.Format("{0} - {1}", purchaseAmount, rate);
            PurchaseAmount = purchaseAmount;
            Rate = rate;
            DiscountType = discountType;
        }
    }
}
