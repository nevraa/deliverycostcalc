﻿using System;

namespace DeliveryCostCalculator.Model
{
    public class CampaignEntity
    {
        public int Id { get; set; }
        public int CategoryId { get; set; }
        public double Rate { get; set; }
        public double Amount { get; set; }
        public DiscountType DiscountType { get; set; }

        public CampaignEntity(int id, int categoryId, double  rate, double amount, DiscountType discountType)
        {
            Id = id;
            CategoryId = categoryId;
            Rate = rate;
            Amount = amount;
            DiscountType = discountType;
        }
    }
}
