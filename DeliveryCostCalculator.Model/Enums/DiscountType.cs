﻿namespace DeliveryCostCalculator.Model
{
    public enum DiscountType
    {
        None = 0,
        Rate,
        Amount
    }
}
