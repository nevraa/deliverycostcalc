﻿namespace DeliveryCostCalculator.Model.Interfaces
{
    public interface IShoppingCart
    {
        double GetTotalAmountAfterDiscounts();
        double GetCouponDiscount();
        double GetCampaignDiscount();
        double GetDeliveryCost();

        void AddItem(ProductEntity product, double amount);

        CampaignEntity ApplyDiscounts(params CampaignEntity[] campaigns);
        void ApplyCoupon(CouponEntity coupon);

        string Print();
    }
}
