﻿using DeliveryCostCalculator.Model;
using DeliveryCostCalculator.Model.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliveryCostCalculator.Calc
{
    public class ShoppingCart : IShoppingCart
    {
        public List<ProductEntity> ProductList;
        public double TotalPrice;
        public double UsedCampaignDiscount;
        public double UsedCouponDiscount;

        public ShoppingCart()
        {
            if (ProductList == null)
                ProductList = new List<ProductEntity>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="product"></param>
        /// <param name="amount"></param>
        public void AddItem(ProductEntity product, double amount)
        {
            for (int i = 0; i < amount; i++)
            {
                ProductList.Add(product);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="coupon"></param>
        public void ApplyCoupon(CouponEntity coupon)
        {
            switch (coupon.DiscountType)
            {
                case DiscountType.None:
                    break;
                case DiscountType.Rate:
                    TotalPrice = TotalPrice >= coupon.PurchaseAmount ? TotalPrice * coupon.Rate : TotalPrice;
                    break;
                case DiscountType.Amount:
                    TotalPrice = TotalPrice >= coupon.PurchaseAmount ? TotalPrice - coupon.Rate : TotalPrice;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="campaigns"></param>
        /// <returns></returns>
        public CampaignEntity ApplyDiscounts(params CampaignEntity[] campaigns)
        {
            Dictionary<int, double> campaignRates = new Dictionary<int, double>();
            var campaignList = campaigns.ToList();
            var groupedProductList = ProductList.GroupBy(x => x.Category.Id).Select(g => new
            {
                CategoryId = g.Key,
                Total = g.Count()
            }).ToList();

            foreach (var item in campaignList)
            {
                if (groupedProductList.Any(x => x.CategoryId == item.CategoryId))
                {
                    var itemCount = groupedProductList.FirstOrDefault(x => x.CategoryId == item.CategoryId).Total;
                    if (itemCount >= item.Amount)
                        campaignRates.Add(item.Id, item.Rate);
                }
            }
            int campaignId = campaignRates.Aggregate((l, r) => l.Value > r.Value ? l : r).Key;
            UsedCampaignDiscount = campaignRates[campaignId];
            return campaigns[campaignId];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetCampaignDiscount()
        {
            return UsedCampaignDiscount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetCouponDiscount()
        {
            return UsedCouponDiscount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetDeliveryCost()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public double GetTotalAmountAfterDiscounts()
        {
            return TotalPrice - (UsedCouponDiscount + UsedCampaignDiscount);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string Print()
        {
            if (ProductList.Any())
            {
                var printedStr = string.Empty;
                var groupedList = ProductList.GroupBy(x => x.Category.Id).Select(g => new
                {
                    CategoryName = ProductList.FirstOrDefault(x => x.Category.Id == g.Key).Category.Title,
                    UnitPrice = ProductList.FirstOrDefault(x => x.Category.Id == g.Key).Price,
                    Quantity = g.Count()
                });

                foreach (var item in groupedList)
                {
                    printedStr += $"Category Name: {item.CategoryName}, Unit Price: {item.UnitPrice}, Quantity: {item.Quantity}\n\n";
                }
                printedStr += $"Total Price: {TotalPrice}, Total Campaign Discount: {UsedCampaignDiscount}, Total Coupon Discount: {UsedCouponDiscount}";
                return "";
            }

            return "Your cart is empty.";
        }
    }
}
