﻿using DeliveryCostCalculator.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DeliveryCostCalculator.Calc
{
    public class DeliveryCostCalculator
    {
        private double CostPerDelivery { get; set; }
        private double CostPerProduct { get; set; }

        public DeliveryCostCalculator(double costPerDelivery, double costPerProduct)
        {
            CostPerDelivery = costPerDelivery;
            CostPerProduct = costPerProduct;
        }

        public double CalculateFor(ShoppingCart cart)
        {
            var groupedCartByCategory = cart.ProductList.GroupBy(x => x.Category.Id).Count();
            var groupedCartByProduct = cart.ProductList.GroupBy(x => x.Id).Count();
            return (CostPerDelivery * groupedCartByCategory) + (CostPerProduct * groupedCartByProduct) + Constants.FixedCost;
        }
    }
}
