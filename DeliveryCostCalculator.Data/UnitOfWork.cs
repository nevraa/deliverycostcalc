﻿using DeliveryCostCalculator.Data.Models;
using System;
using System.Diagnostics;

namespace DeliveryCostCalculator.Data
{
    public class UnitOfWork : IDisposable
    {
        private readonly ShoppingDbContext _context = null;
        private bool _disposed = false;

        private GenericRepository<Product> _productRepository;
        private GenericRepository<Category> _categoryRepository;

        public UnitOfWork()
        {
            _context = new ShoppingDbContext();
        }

        public GenericRepository<Product> ProductRepository => _productRepository ?? (_productRepository = new GenericRepository<Product>(_context));
        public GenericRepository<Category> CategoryRepository => _categoryRepository ?? (_categoryRepository = new GenericRepository<Category>(_context));

        public void Save()
        {
            try
            {
                _context.SaveChanges();
            }
            catch (InvalidOperationException e)
            {
                throw;
            }

        }

        public virtual void Dispose(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                {
                    Debug.WriteLine("UnitOfWork is being disposed");
                    _context.Dispose();
                }
            }
            _disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
