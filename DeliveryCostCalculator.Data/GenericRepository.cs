﻿using DeliveryCostCalculator.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace DeliveryCostCalculator.Data
{
    public class GenericRepository<TEntity> where TEntity : class
    {
        internal ShoppingDbContext _context;
        internal DbSet<TEntity> _dbSet;

        public GenericRepository(ShoppingDbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> Get()
        {
            IQueryable<TEntity> query = _dbSet;
            return query.ToList();
        }

        /// <summary>
        /// Generic get method on the basis of id for Entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual TEntity GetById(object id)
        {
            return _dbSet.Find(id);
        }

        /// <summary>
        /// Generic get method on the basis of id for Entities.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual async Task<TEntity> GetByIdAsync(object id)
        {
            return await _dbSet.FindAsync(id);
        }

        /// <summary>
        /// generic Insert method for the entities
        /// </summary>
        /// <param name="entity"></param>
        public virtual TEntity Insert(TEntity entity)
        {
            _dbSet.Add(entity);
            return entity;
        }


        /// <summary>
        /// Generic Delete method for the entities
        /// </summary>
        /// <param name="id"></param>
        public virtual void Delete(object id)
        {
            var entityToDelete = _dbSet.Find(id);
            Delete(entityToDelete);
        }

        /// <summary>
        /// Generic Delete method for the entities
        /// </summary>
        /// <param name="entityToDelete"></param>
        public virtual void Delete(TEntity entityToDelete)
        {
            if (_context.Entry(entityToDelete).State == EntityState.Detached)
            {
                _dbSet.Attach(entityToDelete);
            }
            _dbSet.Remove(entityToDelete);

        }

        /// <summary>
        /// Generic update method for the entities
        /// </summary>
        /// <param name="entityToUpdate"></param>
        public virtual TEntity Update(TEntity entityToUpdate)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).State = EntityState.Modified;

            return entityToUpdate;
        }

        /// <summary>
        /// Generic update method for the entities with some ignored properties.
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <param name="ignoredProperties"></param>
        /// <returns></returns>
        public virtual TEntity UpdateWithIgnores(TEntity entityToUpdate, List<string> ignoredProperties)
        {
            _dbSet.Attach(entityToUpdate);

            foreach (var item in ignoredProperties)
            {
                _context.Entry(entityToUpdate).Property(item).IsModified = false;
            }
            _context.Entry(entityToUpdate).State = EntityState.Modified;


            return entityToUpdate;
        }

        /// <summary>
        /// Generic update method for only one field.
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <param name="propertyName"></param>
        /// <returns></returns>
        public virtual TEntity UpdateForOneField(TEntity entityToUpdate, string propertyName)
        {
            _dbSet.Attach(entityToUpdate);
            _context.Entry(entityToUpdate).Property(propertyName).IsModified = true;


            return entityToUpdate;
        }

        /// <summary>
        /// Generic update method for only one field.
        /// </summary>
        /// <param name="entityToUpdate"></param>
        /// <param name="propertyNameList"></param>
        /// <returns></returns>
        public virtual TEntity UpdateForFields(TEntity entityToUpdate, List<string> propertyNameList)
        {
            _dbSet.Attach(entityToUpdate);

            foreach (var item in propertyNameList)
            {
                _context.Entry(entityToUpdate).Property(item).IsModified = true;
            }


            return entityToUpdate;
        }

        /// <summary>
        /// generic method to get many record on the basis of a condition.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetMany(Func<TEntity, bool> where)
        {
            return _dbSet.Where(where).ToList();
        }

        /// <summary>
        /// generic method to get many record on the basis of a condition.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetMany(Func<TEntity, bool> where, int take, string propertyName)
        {
            return _dbSet.Where(where).OrderByDescending(x => x.GetType().GetProperty(propertyName).GetValue(x, null)).Take(take).ToList();
        }

        /// <summary>
        /// generic method to get many record on the basis of a condition but query able.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public virtual IQueryable<TEntity> GetManyQueryable(Func<TEntity, bool> where)
        {
            return _dbSet.Where(where).AsQueryable();
        }

        /// <summary>
        /// generic get method , fetches data for the entities on the basis of condition.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public TEntity Get(Func<TEntity, Boolean> where)
        {
            return _dbSet.Where(where).FirstOrDefault();
        }

        /// <summary>
        /// get method with as no tracking, for without update and delete operations.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public TEntity GetAsNoTracking(Func<TEntity, Boolean> where)
        {
            return _dbSet.AsNoTracking().Where(where).FirstOrDefault();
        }

        /// <summary>
        /// generic delete method , deletes data for the entities on the basis of condition.
        /// </summary>
        /// <param name="where"></param>
        /// <returns></returns>
        public void Delete(Func<TEntity, bool> where)
        {
            var objects = _dbSet.Where(where).AsQueryable();
            foreach (TEntity obj in objects)
                _dbSet.Remove(obj);

        }

        /// <summary>
        /// generic method to fetch all the records from db
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAll()
        {
            return _dbSet.ToList();
        }

        /// <summary>
        /// generic method to fetch all the records from db without delete or update operations.
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<TEntity> GetAsNoTrackingAll()
        {
            return _dbSet.AsNoTracking().ToList();
        }

        /// <summary>
        /// Inclue multiple
        /// </summary>
        /// <param name="predicate"></param>
        /// <param name="include"></param>
        /// <returns></returns>
        public IQueryable<TEntity> GetWithInclude(Expression<Func<TEntity, bool>> predicate, params string[] include)
        {
            IQueryable<TEntity> query = _dbSet;
            query = include.Aggregate(query, (current, inc) => current.Include(inc));
            return query.Where(predicate);
        }

        /// <summary>
        /// Generic method to check if entity exists
        /// </summary>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public bool Exists(object primaryKey)
        {
            return _dbSet.Find(primaryKey) != null;
        }

        /// <summary>
        /// Gets a single record by the specified criteria (usually the unique identifier)
        /// </summary>
        /// <param name="predicate">Criteria to match on</param>
        /// <returns>A single record that matches the specified criteria</returns>
        public TEntity GetSingle(Func<TEntity, bool> predicate)
        {

            return _dbSet.AsNoTracking().Single(predicate);
        }

        /// <summary>
        /// The first record matching the specified criteria
        /// </summary>
        /// <param name="predicate">Criteria to match on</param>
        /// <returns>A single record containing the first record matching the specified criteria</returns>
        public TEntity GetFirst(Func<TEntity, bool> predicate)
        {
            return _dbSet.FirstOrDefault(predicate);
        }
    }
}
