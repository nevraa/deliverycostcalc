﻿using System;
using System.Collections.Generic;

namespace DeliveryCostCalculator.Data.Models
{
    public partial class Campaign
    {
        public int Id { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public int DiscountType { get; set; }
        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
