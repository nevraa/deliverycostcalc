﻿using System;
using System.Collections.Generic;

namespace DeliveryCostCalculator.Data.Models
{
    public partial class Coupon
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal PurchaseAmount { get; set; }
        public int DiscountType { get; set; }
    }
}
