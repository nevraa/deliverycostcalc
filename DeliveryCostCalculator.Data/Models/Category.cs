﻿using System;
using System.Collections.Generic;

namespace DeliveryCostCalculator.Data.Models
{
    public partial class Category
    {
        public Category()
        {
            Campaign = new HashSet<Campaign>();
            Product = new HashSet<Product>();
        }

        public int Id { get; set; }
        public string Title { get; set; }

        public ICollection<Campaign> Campaign { get; set; }
        public ICollection<Product> Product { get; set; }
    }
}
